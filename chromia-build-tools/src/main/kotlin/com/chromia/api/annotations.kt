package com.chromia.api

/**
 * Experimantal API that is subject to change
 */
annotation class ExperimentalApi(val reason: String = "")
