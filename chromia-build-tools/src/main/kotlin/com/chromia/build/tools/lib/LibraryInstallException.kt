package com.chromia.build.tools.lib

class LibraryInstallException(msg: String): RuntimeException(msg)
