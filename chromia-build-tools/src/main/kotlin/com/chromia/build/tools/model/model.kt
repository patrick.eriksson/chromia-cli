package com.chromia.cli.model

import net.postchain.rell.base.model.R_LangVersion

const val RellVersion = "0.13.5"
val MinimalRellVersionStrictGtv = R_LangVersion.of("0.13.9")
