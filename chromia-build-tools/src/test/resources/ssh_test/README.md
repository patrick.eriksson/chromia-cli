# SSH Keys
The keys in this resources directory is deploy keys to a specific repository. 
They are read only and restricted to a specific dummy git repository used for test in
this suite. 

Read more: https://docs.github.com/en/authentication/connecting-to-github-with-ssh/managing-deploy-keys#deploy-keys