package com.chromia.cli.command.generate

import com.chromia.cli.tools.config.optionalChromiaModelOption
import com.chromia.rell.dokka.RellDokkaGenerator
import com.chromia.rell.dokka.config.RellDokkaPluginConfigurationBuilder
import com.github.ajalt.clikt.core.CliktCommand
import com.github.ajalt.clikt.parameters.groups.provideDelegate
import com.github.ajalt.clikt.parameters.options.default
import com.github.ajalt.clikt.parameters.options.flag
import com.github.ajalt.clikt.parameters.options.option
import com.github.ajalt.clikt.parameters.options.split
import com.github.ajalt.clikt.parameters.options.validate
import com.github.ajalt.clikt.parameters.types.file
import java.io.File

class GenerateDocsSiteCommand : CliktCommand(
        name = "docs-site",
        help = """
            Generate a documentation site for a dapp ontology
        """.trimIndent()
) {
    private val settings by optionalChromiaModelOption()
    private val docsModel by lazy { settings.model!!.docs }

    private val system by option(hidden = true).flag()
    private val systemIncludes by option("-si", "--system-include", hidden = true)
            .file(mustExist = true, canBeDir = false, mustBeReadable = true)
            .split(",")
            .default(listOf())
            .validate { files -> require(files.all { it.extension == "md" }) { "File(s) must be in markdown format ${files.joinToString { it.path }}" } }

    private val target by option("-d", "--target", help = "Directory to generate code in")
            .file(canBeFile = false)

    override fun run() {
        require(system || settings.model != null) { "Project settings file not found" }
        require(!system || target != null) { "Please specify target folder when generating system docs" }
        val targetFolder = target ?: File(settings.targetDir!!, "site")

        val builder = configBuilder()
                .targetFolder(targetFolder)
        RellDokkaGenerator(builder).generate()
        echo("Documentation generated at $targetFolder")
    }

    private fun configBuilder() = when (system) {
        true -> RellDokkaPluginConfigurationBuilder.SYSTEM.includes(systemIncludes)
        else -> RellDokkaPluginConfigurationBuilder(
                title = docsModel.title,
                modules = settings.model!!.blockchains.values.map { it.module },
                projectRoot = settings.sourceDir!!
        )
                .customStyleSheets(docsModel.customStyleSheets)
                .customAssets(docsModel.customAssets)
                .includes(docsModel.additionalContentFiles)
                .footerMessage(docsModel.footerMessage)
    }
}
