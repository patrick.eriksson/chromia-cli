package com.chromia.cli.command

import com.chromia.cli.model.ChromiaModel
import com.chromia.cli.tools.config.optionalChromiaModelOption
import com.chromia.cli.tools.env.CliktCliEnv
import com.chromia.cli.util.logSqlOption
import com.chromia.cli.util.module
import com.github.ajalt.clikt.core.CliktCommand
import com.github.ajalt.clikt.core.CliktError
import com.github.ajalt.clikt.core.PrintMessage
import com.github.ajalt.clikt.parameters.groups.provideDelegate
import com.github.ajalt.clikt.parameters.options.flag
import com.github.ajalt.clikt.parameters.options.option
import com.github.ajalt.clikt.parameters.types.file
import com.google.common.base.Throwables
import java.io.File
import net.postchain.rell.api.base.RellApiCompile
import net.postchain.rell.api.shell.RellApiRunShell
import net.postchain.rell.base.compiler.base.utils.C_Message
import net.postchain.rell.base.repl.ReplInputChannel
import net.postchain.rell.base.repl.ReplInputChannelFactory
import net.postchain.rell.base.repl.ReplOutputChannel
import net.postchain.rell.base.repl.ReplOutputChannelFactory
import net.postchain.rell.base.repl.ReplValueFormat
import net.postchain.rell.base.repl.ReplValueFormatter
import net.postchain.rell.base.runtime.Rt_Exception
import net.postchain.rell.base.runtime.Rt_Value
import net.postchain.rell.base.runtime.utils.Rt_Utils


class ReplCommand : CliktCommand(help = """
    REPL is used to create a language shell for Rell that takes single user inputs, executes them, and returns the result.
    Inside the repl you can create local variables and execute Rell commands, it can be attached to a Rell module to be able
    to inspect a dapp state and execute dapp functionalities.
    
    Run query commands:
    To be able to run queries in the shell, a user must have a module defined from the start of the repl command in 
    which the query is defined. Queries that do not depend on entities can be executed without a database connection, and queries
    that depend on an entity must then have a database connection defined.
    
    Run Operations commands:
    When a operation is executed from the repl shell, the database connection and a module needs to be defined from the start of the repl command,
    to be able to execute  an operation it needs added to a transaction. 
    This can be done by wrapping it with a test transaction like this: `rell.test.tx(<your operation>..).run()`
""".trimIndent()) {
    private val settings by optionalChromiaModelOption()
    private val sourceDir by lazy { settings.sourceDir ?: File(System.getProperty("user.dir")) }
    private val module by module()
    private val sqlLog by logSqlOption()
    private val historyFile by option(help = "Save command history to this file").file(canBeDir = false, mustBeWritable = true)
    private val useDB by option(help = "If a session towards the configured database should be established").flag()
    private val command by option("-c", "--command", help = "Execute a single command", metavar = "COMMAND")
    private val rawOutput by option("-r", "--raw-output", help = "Will print large object line by line and strings without quotes").flag()

    override fun run() {

        if (module != null && settings.model == null) {
            echo("To find the module \"$module\", specifying the settings file is required")
            return
        }

        if (useDB && settings.model == null) {
            throw CliktError("To correctly connect to the database, specifying the settings file is required")
        }
        val localModel = settings.model ?: ChromiaModel.default()
        val compileConfig = RellApiCompile.Config.Builder()
                .cliEnv(CliktCliEnv(this))
                .mountConflictError(false)
                .quiet(localModel.compile.quiet)
                .build()

        val shellConfig = RellApiRunShell.Config.Builder()
                .apply { if (!command.isNullOrBlank()) inputChannelFactory(IteratorCommandInputChannelFactory(listOf(command!!))) }
                .compileConfig(compileConfig)
                .databaseUrl(if (useDB) "${localModel.databaseUrl}&currentSchema=${localModel.databaseSchema}" else null)
                .historyFile(historyFile)
                .outPrinter(::echo)
                .logPrinter(::echo)
                .outputChannelFactory(CliktOutputChannelFactory(!command.isNullOrBlank()))
                .sqlErrorLog(localModel.logSqlErrors)
                .sqlLog(sqlLog)
                .printIntroMessage(command.isNullOrBlank())
                .build()
        RellApiRunShell.runShell(shellConfig, sourceDir, module?.str())
    }

    private class IteratorCommandInputChannelFactory(val commands: Iterable<String>) : ReplInputChannelFactory {
        override fun createInputChannel(historyFile: File?) = object : ReplInputChannel {
            val commandIterator = commands.iterator()
            override fun readLine(prompt: String) = if (commandIterator.hasNext()) commandIterator.next() else null
        }
    }

    private inner class CliktOutputChannelFactory(private val failOnError: Boolean) : ReplOutputChannelFactory {
        private var valueFormat = if (rawOutput) ReplValueFormat.ONE_ITEM_PER_LINE else ReplValueFormat.GTV_STRING
        override fun createOutputChannel() = object : ReplOutputChannel {
            override fun printInfo(msg: String) = echo(msg)
            override fun printCompilerError(code: String, msg: String) = if (failOnError) throw PrintMessage(msg, 1) else echo(msg, err = true)
            override fun printCompilerMessage(message: C_Message) = if (failOnError) throw PrintMessage(message.toString(), 1) else echo(message)
            override fun printControl(code: String, msg: String) = echo(msg)
            override fun printPlatformRuntimeError(e: Throwable) {
                val message = "Run-time error: " + Throwables.getStackTraceAsString(e).trim()
                if (failOnError) throw PrintMessage(message, 1)
                echo(message)
            }

            override fun printRuntimeError(e: Rt_Exception) {
                val message = Rt_Utils.appendStackTrace("Run-time error: ${e.message}", e.info.stack)
                if (failOnError) throw PrintMessage(message, 1)
                echo(message)
            }

            override fun printValue(value: Rt_Value) {
                ReplValueFormatter.format(value, valueFormat)?.let { echo(it) }
            }

            override fun setValueFormat(format: ReplValueFormat) {
                valueFormat = format
            }
        }
    }
}
